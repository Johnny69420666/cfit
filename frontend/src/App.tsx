import * as React from "react";
import {useEffect, useState} from "react";
import {navigate, useRoutes} from "hookrouter";
import HeaderComponent from "./components/HeaderComponent"
import ErrorComponent from "./components/ErrorComponent";
import LoginForm from "./components/LoginForm";
import Admin from "./screens/AdminPanel/Admin";
import AppCtx from "./contexts/AppCtx"
import AuthCtx from "./contexts/AuthCtx";
import "./App.css";
import {UserDto} from "./types/user";
import Home from "./components/Home";
import User from "./screens/UserPanel/User";
import Auth from "./components/Auth";
import ChangePassword from "./screens/UserPanel/ChangePassword/ChangePassword";

require('dotenv').config();

export default function App() {

  const [state, setState] = useState<{user: UserDto | null}>({user: null})
  const [authState, setAuthState] = useState<{authenticated: boolean}>({authenticated: false})

  function handleUserLogin(user: UserDto) {
    localStorage.setItem('user', JSON.stringify(user))
    setState({user:user})
  }

  function updateUser(user: UserDto) {
      setState({user:user})
  }

  function handleAuth() {
      console.log("handle auth XD")
      localStorage.setItem('admin', JSON.stringify({authenticated: true}))
      setAuthState({authenticated: true})
      navigate('/admin')
  }

  useEffect(() => {
      const user = localStorage.getItem('user')
      const admin = localStorage.getItem('admin')
      if(user != null){
          setState({user: JSON.parse(user)})
          console.log('app effect user' + user)
      }
      if (admin != null){
          setAuthState(JSON.parse(admin))
          console.log('app effect admin')
      }
  },[])

  const routes = {
    '/': () => <Home/>,
    '/auth': () => <Auth/>,
    '/admin*': () => <Admin/>,
    '/login': () => <LoginForm onUserLogin={handleUserLogin}/>,
    '/dashboard': () => <User/>,
    '/change': () => <ChangePassword/>,
  };

  const routeResult = useRoutes(routes)

  return (
    <div className="App">
      <AppCtx.Provider value={{user: state.user, updateUser}}>
        <AuthCtx.Provider value={{authenticated: authState.authenticated, updateContext: handleAuth}}>
          <HeaderComponent />
          {routeResult || <ErrorComponent />}
        </AuthCtx.Provider>
      </AppCtx.Provider>
    </div>
  );
}