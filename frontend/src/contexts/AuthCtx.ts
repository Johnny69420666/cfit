import { createContext } from 'react'

const AuthCtx = createContext<{authenticated: boolean, updateContext: (() => void) | null}>({authenticated: false, updateContext: null})

export default AuthCtx