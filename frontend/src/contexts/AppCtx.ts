import { createContext } from "react";
import { UserDto } from "../types/user";

const AppCtx = createContext<{user: UserDto | null, updateUser: (UserDto) => void}>({user: null, updateUser: (asdf) => {}});

export default  AppCtx
