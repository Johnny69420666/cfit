import React, {useContext, useState} from 'react'
import {Field, Form, Formik} from 'formik'
import {Alert, Button, Col, Container} from "reactstrap";
import AppCtx from "../../../contexts/AppCtx";
import axios from 'axios'
import {navigate} from 'hookrouter'
import LoggedInError from "../../../components/LoggedInError";

const ChangePassword:React.FC = () => {

    const [matchError, setMatchError] = useState(false)
    const [shortError, setShortError] = useState(false)

    const ctx = useContext(AppCtx)

    function handleChangePassword(val, bag) {
        bag.setSubmitting(true)
        if (val.password.length < 5){
            bag.setSubmitting(false)
            setShortError(true);
            return;
        }
        if(val.password != val.confirmpw || ctx.user == null){
            bag.setSubmitting(false)
            setMatchError(true)
            return
        }
        axios.post(process.env.REACT_APP_API_URL+'/api/user/change', {password: val.password, id:ctx.user.id})
            .then((response) => {
                console.log(response);
                navigate('/dashboard')
            })
            .catch((err) => {
                console.log(err);
            })
    }

    return(
        ctx.user?(
        <Container>
            <Col>
                <div>
                <Formik
                    onSubmit={handleChangePassword}
                    initialValues={{password:'',confirmpw:''}}
                    render={({errors, status, touched, isSubmitting}) =>
                        <Form>
                            <Field type="password" name="password" placeholder="Nova Zaporka"/>
                            <Field type="password" name="confirmpw" placeholder="Potvrdi Zaporku"/>
                            <Button type="submit" disabled={isSubmitting}>Potvrdi</Button>
                        </Form>
                    }
                />
                </div>
                {matchError &&
                    <Alert color="danger">
                            Zaporke nisu jednake
                    </Alert>
                }
                {shortError &&
                    <Alert color="danger">
                        Zaporka treba sadržavat više od 6 znakova
                    </Alert>
                }
            </Col>
        </Container>
        ) : (
            <LoggedInError/>
        )
    );
}

export default ChangePassword