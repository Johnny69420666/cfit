import React, {useEffect, useState} from 'react'
import {SessionDto} from "../../../types/session";
import axios from "axios";
import {Card, CardBody, Collapse, Spinner, Table} from "reactstrap";
import Button from "reactstrap/lib/Button";
import Users from './Users/Users'
import {Field, Form, Formik} from "formik";
import {format} from "date-fns";

const Sessions:React.FC = () => {

    const [sessions, setSessions] = useState<[SessionDto] | null>(null);
    const [collapse, setCollapse] = useState(false);
    const [collapseNew, setCollapseNew] = useState(false);
    const [err, setErr] = useState();

    function handleDeleteSession(id){
        axios.delete(process.env.REACT_APP_API_URL+'/api/session/delete/'+id)
            .then((response) => {
                console.log(response+ 'deleted session');
                getSessions()
            })
            .catch((err) => {
                console.log(err);
            })
    }

    function handleDecrement(id){
        axios.post(process.env.REACT_APP_API_URL+'/api/session/decrement/' + id)
            .then((response) => {
                console.log(response+ 'decrement');
                getSessions()
            })
            .catch((err) => {
                console.log(err);
            })
    }

    function handleIncrement(id){
        axios.post(process.env.REACT_APP_API_URL+'/api/session/increment/' + id)
            .then((response) => {
                console.log(response + 'increment');
                getSessions()
            })
            .catch((err) => {
                console.log(err);
            })
    }

    function toggle() {
        setCollapse(!collapse);
    }

    function toggleNew() {
        setCollapseNew(!collapseNew);
    }

    function getSessions(){
        let isSubscribed = true;
        axios.get(process.env.REACT_APP_API_URL+'/api/session/all')
            .then((response) => isSubscribed? (setSessions(response.data)) : null)
            .catch(error => isSubscribed? (setErr(error)) : null);
        return () => {isSubscribed = false}
    }

    function handleNewSession(val, bag) {
        console.log(val);
        bag.setSubmitting(false);
        setCollapseNew(false);
        axios.post(process.env.REACT_APP_API_URL+'/api/session/new',val && {datetime: val.date + " " + val.time})
            .then((response) => {
                console.log(response+ 'new session');
                getSessions();
            })
            .catch((err) => {
                console.log(err);
            })
    }

    useEffect(() => {
        let isSubscribed = true;
        axios.get(process.env.REACT_APP_API_URL+'/api/session/all')
            .then((response) => isSubscribed? (setSessions(response.data)) : null)
            .catch(error => isSubscribed? (setErr(error)) : null);
        return () => {isSubscribed = false}
    },[]);

    return sessions?(<>
        <Button color="success" onClick={toggleNew} style={{ marginBottom: '1rem' }}>New Session</Button>
        <Collapse isOpen={collapseNew}>
            <Card>
                <CardBody>
                    <Formik
                        onSubmit={handleNewSession}
                        initialValues={null}
                        render={({errors, status, touched, isSubmitting}) =>
                            <Form>
                                <Field type="date" name="date"/>
                                <Field type="time" name="time"/>
                                <Button type="submit" disabled={isSubmitting}>Submit</Button>
                            </Form>
                        }
                    />
                </CardBody>
            </Card>
        </Collapse>
        <div className="kontenjer">
            <Table bordered size="sm">
                <thead>
                <tr>
                    <th>Sessions</th>
                    <th><Button onClick={() => {toggle()}}>View Users</Button></th>
                </tr>
                </thead>
                <tbody>
                {sessions?(
                    sessions.map(session =>
                        <tr key={session.id}>
                            <td><Button color="danger" onClick={() => handleDeleteSession(session.id)}>x</Button></td>
                            <td className="specialCell">
                                <Button color="info" onClick={() => handleDecrement(session.id)}>-</Button>
                                {session.numOfAllowedUsers}
                                <Button color="success" onClick={() => handleIncrement(session.id)}>+</Button>
                            </td>
                            <td>{format(session.date,'dd.MM HH:mm')}</td>
                            <Collapse isOpen={collapse}>
                                <Card>
                                    <CardBody>
                                        <Users id={session.id}/>
                                    </CardBody>
                                </Card>
                            </Collapse>
                        </tr>)
                ):(
                    <td>
                        <Spinner color="dark" style={{ width: '2rem', height: '2rem' }}  />
                    </td>
                )
                }
                </tbody>
            </Table>
        </div>
    </>) : (<>
        <Spinner color="primary" />
    </>)
}

export default Sessions