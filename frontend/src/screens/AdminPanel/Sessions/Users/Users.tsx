import React, {useEffect, useState} from 'react'
import axios from "axios";
import {UserDto} from "../../../../types/user";
import {ListGroup, ListGroupItem} from "reactstrap";

type Props = {id: number}

const Users:React.FC<Props> = (props) =>{

    const [users, setUsers] = useState<UserDto[]>([])

    useEffect(() => {
        let isSubscribed = true
        axios.get(process.env.REACT_APP_API_URL+'/api/session/'+props.id)
            .then((response) => isSubscribed? (setUsers(response.data)) : (null))
            .catch((err) => isSubscribed? (console.log(err)) : (null))
        return () => {isSubscribed = false}
    },[users.map(u => u.id).join(',')]);



    return(
        <ListGroup>
            {users && users.map((user) =>
            <>
                <ListGroupItem>{user.firstName}  {user.lastName}</ListGroupItem>
            </>
            )}
        </ListGroup>
    );
}

export default Users