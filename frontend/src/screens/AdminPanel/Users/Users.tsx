import React, {useEffect} from 'react'
import {useState} from "react";
import {UserDto} from "../../../types/user";
import axios from "axios";
import {navigate} from 'hookrouter';
import {Button, Spinner, Input, Table} from "reactstrap";

const Users:React.FC = () => {

    const [users, setUsers] = useState<[UserDto] | null>(null)
    const [error, setError] = useState<String | null>(null)
    const [search, setSearch] = useState('')

    function getUsers(){
        axios.get(process.env.REACT_APP_API_URL+'/api/user')
            .then((response) => {
                console.log(response);
                setUsers(response.data)
            })
            .catch(error => {
                console.log(error);
            });
    }

    function searchUsers(email){
        axios.post(process.env.REACT_APP_API_URL+'/api/user/search', {email})
            .then((response) => {
                setUsers(response.data)
            })
    }

    function handleEditClicked(id){
        navigate('/admin/edit/' + id)
    }

    function handleSearchChange(e){
        setSearch(e.target.value)
        searchUsers(e.target.value)
        if (e.target.value === ''){
            getUsers()
        }
    }

    useEffect(() => {
        let isSubscribed: boolean = true;
        axios.get(process.env.REACT_APP_API_URL+'/api/user')
            .then((response) => isSubscribed? (setUsers(response.data)): null )
            .catch(error => isSubscribed? (setError(error.toString())): null );
        return () => {isSubscribed = false}
    }, []);

    return users ? (<>
        <Input
            value={search}
            onChange={handleSearchChange}
            type="text"
            placeholder='Search for users here'
            className="userSearchBox"
        />
        <div className="adminUserTable">
            <Table bordered size="sm">
                <tbody>
                {users && users.map(user =>
                    <tr key={user.id}>
                        <td>{user.firstName}  {user.lastName}</td>
                        <td><Button color="primary" onClick={() => handleEditClicked(user.id)}>Edit</Button></td>
                    </tr>
                )}
                </tbody>
            </Table>
        </div>
    </>) : (<>
        <Spinner color="primary" />
    </>)
}

export default Users