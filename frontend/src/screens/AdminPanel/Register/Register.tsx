import React, { useState } from 'react'
import {Button, Container, Col, Row} from 'reactstrap';
import {navigate} from 'hookrouter'
import axios from 'axios'

import '../../../components/style/RegisterUserStyle.css'
import {Field, Form, Formik} from "formik";
import {RegistrationDto} from "../../../types/registration";

const Register:React.FC = () =>{

    const [reg,setReg]= useState<RegistrationDto | null>(null)

    function handleSubmit(val, bag){
        console.log(val)
        axios.post(process.env.REACT_APP_API_URL+'/api/user/new', val)
            .then(function (response) {
                console.log(response);
                bag.setSubmitting(false)
                navigate('/admin');
            })
            .catch(function (response) {
                console.log(response);
            });
    }

    return(
    <>
    <Container>
        <Row>
            <Col>
            <Formik
                onSubmit={handleSubmit}
                initialValues={reg}
                render={({errors, status, touched, isSubmitting}) =>
                        <Form>
                            <Field type="text" name="firstName" placeholder="Fname"/>
                            <Field type="text" name="lastName" placeholder="Lname"/>
                            <Field type="email" name="email" placeholder="Email"/>
                            <Button type="submit" disabled={isSubmitting}>Submit</Button>
                        </Form>
                }
            />
            </Col>
        </Row>
    </Container>
    </>
    );
}

export default Register;