import React, {useContext} from 'react'
import {
    Button,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavItem,
    UncontrolledButtonDropdown,
} from 'reactstrap';
import {navigate} from 'hookrouter'
import {FiUser} from 'react-icons/fi'
import AppCtx from "../contexts/AppCtx";
import {format} from "date-fns";

const HeaderComponent:React.FC = () => {

    const ctx = useContext(AppCtx)

    function handleProfileClick(){
        navigate('/dashboard')
    }

    function handleLoginClicked(){
        navigate('/login')
    }

    function handleBrandClicked(){
        navigate('/')
    }

    function handleChangePassword() {
        navigate('/change')
    }

    function handleUserClicked() {
        navigate('/dashboard')
    }

    function handleLogoutClicked(){
        ctx.updateUser(null)
        localStorage.removeItem('user')
        navigate('/')
    }

    return(
        <>     
        <Navbar color="light" expand="md" fixed="absolute" className="cfitNavbar">
            <NavbarBrand onClick={handleBrandClicked}><div className="logoDva"></div></NavbarBrand>
            <NavbarBrand onClick={handleBrandClicked}><div className="logoContainer"></div></NavbarBrand>
            <NavbarBrand >{ ctx.user && <FiUser onClick={handleUserClicked} size="2em"/>}</NavbarBrand>
            <NavbarBrand style={{marginBottom:"0",marginTop:"5px"}}>
                <p className="headerText">{ ctx.user &&
                <>
                    {ctx.user.firstName}  {ctx.user.lastName} <p> {ctx.user.subscription && "Članarina vrijedi do "+format(ctx.user.subscription,'dd.MM.yyyy')}</p>
                </>}
                </p>
            </NavbarBrand>
            <Nav  className="ml-auto">
                {(ctx.user == null) &&
                <NavItem>
                    <Button color="secondary" onClick={handleLoginClicked}>Login</Button>
                </NavItem>}
                {(ctx.user != null) && <>
                <UncontrolledButtonDropdown>
                    <DropdownToggle caret style={{marginLeft:"100px"}}>
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem onClick={handleProfileClick}>Profil</DropdownItem>
                        <DropdownItem onClick={handleChangePassword}>Promjeni zaporku</DropdownItem>
                        <DropdownItem onClick={handleLogoutClicked}>Odjavi se</DropdownItem>
                    </DropdownMenu>
                </UncontrolledButtonDropdown>
                </>}
            </Nav>
        </Navbar>
        </>
    );
}

export default HeaderComponent