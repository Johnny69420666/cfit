import * as React from 'react';
import {Jumbotron} from 'reactstrap';

const ErrorComponent:React.FC = () =>{
    return(
        <>
        <Jumbotron>
            <h1 className="display-3">404</h1>
            <p className="lead">We could'nt find that</p>
        </Jumbotron>
        </>
    );
}

export default ErrorComponent;