package com.cfit.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CfitApplication {

	public static void main(String[] args) {
		SpringApplication.run(CfitApplication.class, args);
	}
}
