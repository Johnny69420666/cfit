package com.cfit.app.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class MailComponent {

    private JavaMailSender sender;

    @Autowired
    public MailComponent(JavaMailSender sender) {
        this.sender = sender;
    }

    public void sendEmail(String pw, String email) throws Exception{
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setTo(email);
        helper.setText(
                "Hvala što se izabrali Improve.\n" +
                "Vaša zaporka je " + pw +".\n" +
                "Zaporku možete mijenjati na Vašim korisničkim stranicama na linku https://improve-spot.com/\n" +
                "Također, na korisničkim stranicama možete izvršiti prijavu na trening, pratiti arhivu vaših treninga i rezultate funkcionalne dijagnostike. \n" +
                "Za sva pitanja stojimo na raspolaganju. \n" +
                "Vidimo se na treningu.");
        helper.setSubject("Registracija");

        sender.send(message);
    }

    public void deletedSessionMail(String email) throws MessagingException {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setTo(email);
        helper.setText("Poštovani, \n" +
                "termin na koji ste bili prijavljeni je otkazan. Možete se prijaviti na neki od slobodnih termina. \n" +
                "Hvala na razumijevanju.");
        helper.setSubject("Termin");

        sender.send(message);
    }
}
