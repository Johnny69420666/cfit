package com.cfit.app.services;

import com.cfit.app.commands.UserCommand;
import com.cfit.app.commands.WorkoutCommand;
import com.cfit.app.models.Session;
import com.cfit.app.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User findById(Long l);

    List<User> findAll();

    UserCommand registerUser(String firstName, String lastName, String email) throws Exception;

    UserCommand updateUser(Long id, String firstName, String lastName, String email, String subscription);

    List<User> getByNameContaining(String s);

    boolean auth(String email, String password);

    void addWorkout(Long id, WorkoutCommand command);

    Optional<User> login(String email, String password);

    User cancelSession(Long userId, Long sessionId);

    void changePassword(String s, Long id);

    void deleteUser(Long id);

    void saveImageFile(Long id, MultipartFile file);

    void updateDiagnostics(Long id, String diag);
}
