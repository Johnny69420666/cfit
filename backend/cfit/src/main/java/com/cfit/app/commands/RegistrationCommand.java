package com.cfit.app.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RegistrationCommand {
    private String firstName;
    private String lastName;
    private String email;
}
