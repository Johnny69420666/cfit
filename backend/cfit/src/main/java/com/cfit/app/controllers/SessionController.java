package com.cfit.app.controllers;

import com.cfit.app.commands.IdCommand;
import com.cfit.app.commands.NewSessionCommand;
import com.cfit.app.commands.SessionRegisterCommand;
import com.cfit.app.commands.SessionCommand;
import com.cfit.app.models.Session;
import com.cfit.app.models.User;
import com.cfit.app.services.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.List;

@RestController
@RequestMapping("/api/session")
public class SessionController {

    private final SessionService sessionService;

    @Autowired
    public SessionController(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @GetMapping("/all")
    public List<Session> getAll(){
        return sessionService.findAllAndSort();
    }

    @PostMapping("/available")
    public List<SessionCommand> getAvailable(@RequestBody IdCommand command){
        return sessionService.findAvailable(command.getUserId());
    }

    @GetMapping("/{id}")
    public List<User> usersInSession(@PathVariable Long id){
        return sessionService.getUsersInSession(id);
    }

    @PostMapping("/register")
    public List<Session> registerUser(@RequestBody SessionRegisterCommand sessionRegisterCommand){
        return sessionService.registerUser(sessionRegisterCommand.getSessionId(), sessionRegisterCommand.getUserId());
    }

    @PostMapping("/new")
    public void newSession(@RequestBody NewSessionCommand command){
        sessionService.newSession(command);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteSession(@PathVariable Long id) throws MessagingException {
        sessionService.deleteSession(id);
    }

    @PostMapping("/increment/{id}")
    public void incrementSession(@PathVariable Long id){
        sessionService.incrementNumOfUsers(id);
    }

    @PostMapping("/decrement/{id}")
    public void decrement(@PathVariable Long id){
        sessionService.decrementNumOfUsers(id);
    }

}
