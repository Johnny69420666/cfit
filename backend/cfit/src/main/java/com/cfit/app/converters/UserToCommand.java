package com.cfit.app.converters;

import com.cfit.app.commands.UserCommand;
import com.cfit.app.models.User;
import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class UserToCommand implements Converter<User, UserCommand> {

    @Synchronized
    @Nullable
    @Override
    public UserCommand convert(User source){

        if (source == null){
            return null;
        }

        final UserCommand command = new UserCommand();
        command.setFirstName(source.getFirstName());
        command.setLastName(source.getLastName());
        command.setEmail(source.getEmail());
        command.setDiagnostics(source.getDiagnostics());
        return command;
    }
}
