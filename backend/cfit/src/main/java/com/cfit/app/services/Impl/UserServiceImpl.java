package com.cfit.app.services.Impl;

import com.cfit.app.commands.UserCommand;
import com.cfit.app.commands.WorkoutCommand;
import com.cfit.app.components.MailComponent;
import com.cfit.app.converters.CommandToWorkout;
import com.cfit.app.converters.UserToCommand;
import com.cfit.app.models.Session;
import com.cfit.app.models.User;
import com.cfit.app.models.Workout;
import com.cfit.app.repositories.SessionRepository;
import com.cfit.app.repositories.UserRepository;
import com.cfit.app.repositories.WorkoutRepository;
import com.cfit.app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final PasswordEncoder passwordEncoder;
    private final SessionRepository sessionRepository;
    private final UserRepository userRepository;
    private final WorkoutRepository workoutRepository;
    private final UserToCommand userToCommand;
    private final CommandToWorkout commandToWorkout;
    private final MailComponent mailComponent;

    @Autowired
    public UserServiceImpl(PasswordEncoder passwordEncoder, SessionRepository sessionRepository, UserRepository userRepository, WorkoutRepository workoutRepository, UserToCommand userToCommand, CommandToWorkout commandToWorkout, MailComponent mailComponent) {
        this.passwordEncoder = passwordEncoder;
        this.sessionRepository = sessionRepository;
        this.userRepository = userRepository;
        this.workoutRepository = workoutRepository;
        this.userToCommand = userToCommand;
        this.commandToWorkout = commandToWorkout;
        this.mailComponent = mailComponent;
    }

    private Date parseDate(String date, String format) throws ParseException
    {
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.parse(date);
    }

    @Override
    public User findById(Long l){
        Optional<User> userOptional = userRepository.findById(l);
        if (!userOptional.isPresent()){
            throw new RuntimeException("User with id " + l + "not found");
        }
        return userOptional.get();
    }

    @Override
    public List<User> findAll(){
        return userRepository.findAll();
    }

    @Override
    public List<User> getByNameContaining(String s){
        return userRepository.findByFirstNameContainingOrLastNameContaining(s,s);
    }

    @Override
    public boolean auth(String email, String pass){
        if(pass.equals("baguvix_1234") && email.equals("ivan")){
            return true;
        }
        else {return false;}
    }

    @Override
    @Transactional
    public UserCommand registerUser(String firstName, String lastName, String email){
        User detachedUser = new User();
        detachedUser.setFirstName(firstName);
        detachedUser.setLastName(lastName);
        detachedUser.setEmail(email);

        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(10);
        for (int i = 0; i < 10; i++) {
            int index
                    = (int)(AlphaNumericString.length()
                    * Math.random());
            sb.append(AlphaNumericString
                    .charAt(index));
        }
        try {
            mailComponent.sendEmail(sb.toString(), email);
        } catch (Exception e) {
            e.printStackTrace();
        }

        detachedUser.setPassword(sb.toString());

        User savedUser = userRepository.save(detachedUser);
        return userToCommand.convert(savedUser);
    }

    @Override
    @Transactional
    public UserCommand updateUser(Long id, String firstName, String lastName, String email, String subscription){
        User user = userRepository.findById(id).get();

        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        if (subscription != null){
            try {
                user.setSubscription(parseDate(subscription,"yyyy-MM-dd"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }else{
            user.setSubscription(null);
        }
        userRepository.save(user);

        return userToCommand.convert(user);
    }

    @Override
    @Transactional
    public void addWorkout(Long id, WorkoutCommand command){

        User user = userRepository.findById(id).get();
        Workout workout = commandToWorkout.convert(command);

        workout.setDate(new Date());
        user.getWorkouts().add(workout);
        workout.setUser(user);
        workoutRepository.save(workout);
        userRepository.save(user);
    }

    @Override
    public Optional<User> login(String email, String password){
        return userRepository.findByEmailAndPassword(email, password);
    }

    @Override
    public User cancelSession(Long userId, Long sessionId){
        User user = findById(userId);
        try {
            Session session = sessionRepository.findById(sessionId).get();
            user.getSessions().remove(session);
            session.setNumOfAllowedUsers(session.getNumOfAllowedUsers()+1);
            userRepository.save(user);
            sessionRepository.save(session);
        }catch (Exception e){
            System.out.println("XD");
        }

        return user;
    }

    @Override
    public void changePassword(String s, Long id){
        User user = findById(id);
        user.setPassword(s);
        userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id){
        User user = findById(id);
        userRepository.delete(user);
    }

    @Override
    @Transactional
    public void saveImageFile(Long id, MultipartFile file){
        try {

            User user = findById(id);
            Byte[] byteObjects = new Byte[file.getBytes().length];

            int i = 0;
            for (byte b : file.getBytes()){
                byteObjects[i++] = b;
            }

            user.setImage(byteObjects);
            userRepository.save(user);
        }catch (IOException e){
            System.out.println("Error occured" + e);
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void updateDiagnostics(Long id, String diag){
        User user = findById(id);
        user.setDiagnostics(diag);
        userRepository.save(user);
    }
}
