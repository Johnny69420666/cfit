package com.cfit.app.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.Constraint;
import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "SESSION")
public class Session {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date date;
    @Min(0)
    private Integer numOfAllowedUsers;

    @ManyToMany(mappedBy = "sessions")
    @JsonIgnore
    private List<User> users = new ArrayList<>();

    public Session(){
        this.date = new Date();
        this.numOfAllowedUsers = 6;
    }

    public Integer getNumOfAllowedUsers() {
        return numOfAllowedUsers;
    }

    public void setNumOfAllowedUsers(Integer numOfAllowedUsers) {
        this.numOfAllowedUsers = numOfAllowedUsers;
    }

    public Long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Session)) return false;
        Session session = (Session) o;
        return getId().equals(session.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
