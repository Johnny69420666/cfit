package com.cfit.app.components;


import com.cfit.app.services.SessionService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;


@Component
public class ScheduledTasks {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    private static SessionService sessionService;

    public ScheduledTasks(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    @Scheduled(cron = "5 0 0 ? * SUN")
    public void sundayCorn(){
        System.out.println("sunday cron creating sessions for monday/tuesday " + dateTimeFormatter.format(LocalTime.now()));
        sessionService.createNewAM(1);
        sessionService.createNewPM(1);
        sessionService.createNewAM(2);
        sessionService.createNewPM(2);
    }

    @Scheduled(cron = "5 0 0 ? * MON,TUE,WED")
    public void lingeringCorn(){
        System.out.println("Creating session two days ahead " + dateTimeFormatter.format(LocalTime.now()));
        sessionService.createNewAM(2);
        sessionService.createNewPM(2);
    }

    @Scheduled(cron = "5 0 0 ? * THU")
    public void thursdayCorn(){
        System.out.println("Creating saturday " + dateTimeFormatter.format(LocalTime.now()));
        sessionService.createNewSaturday();
    }

    @Scheduled(cron = " 5 0 0/1 ? * * ")
    public void deleteOld() {
        System.out.println("DeleteSessions sessions triggered " + dateTimeFormatter.format(LocalTime.now()));
        sessionService.deleteOld();
    }

}
