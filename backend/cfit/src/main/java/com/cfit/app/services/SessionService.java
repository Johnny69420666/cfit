package com.cfit.app.services;

import com.cfit.app.commands.NewSessionCommand;
import com.cfit.app.commands.SessionCommand;
import com.cfit.app.models.Session;
import com.cfit.app.models.User;

import javax.mail.MessagingException;
import java.util.List;

public interface SessionService {

    Session findById(Long l);

    void deleteOld();

    void createNewAM(int n);

    void createNewPM(int n);

    void createNewSaturday();

    List<Session> findAllAndSort();

    List<SessionCommand> findAvailable(Long userId);

    List<Session> registerUser(Long sessionId, Long userId);

    List<User> getUsersInSession(Long session_id);

    void deleteSession(Long l) throws MessagingException;

    void newSession(NewSessionCommand command);

    void incrementNumOfUsers(Long id);

    void decrementNumOfUsers(Long id);
}
