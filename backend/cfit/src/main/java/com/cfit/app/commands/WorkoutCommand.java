package com.cfit.app.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class WorkoutCommand {
    private String name;
    private String description;
    private Integer reps;
    private Integer sets;
    private Double kgs;
}
