package com.cfit.app.controllers;

import com.cfit.app.commands.*;
import com.cfit.app.models.Session;
import com.cfit.app.models.User;
import com.cfit.app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private UserService userService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserController(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping
    public List<User> getAllUsers(){
        return userService.findAll();
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable Long id){ return userService.findById(id); }

    @PostMapping("/auth")
    public boolean authenticate(@RequestBody LoginCommand command){
       return userService.auth(command.getEmail(), command.getPassword());
    }

    @PostMapping("/login")
    public User loginUser(@RequestBody LoginCommand loginCommand){
        Optional<User> userOptional = userService.login(loginCommand.getEmail(), loginCommand.getPassword());

        if (!userOptional.isPresent()){
            throw new ResponseStatusException(
                    HttpStatus.UNAUTHORIZED
            );
        }

        return userOptional.get();
    }

    @PostMapping("/search")
    public List<User> getUsersContaining(@RequestBody LoginCommand command){
        return userService.getByNameContaining(command.getEmail());
    }

    @PostMapping("/new")
    public void register(@RequestBody UserCommand command){
        try {
            userService.registerUser(command.getFirstName(), command.getLastName(), command.getEmail());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @PostMapping("/update")
    public void updateUser(@RequestBody UserCommand command){
        System.out.println("Updating user " + command.getFirstName());
        userService.updateUser(command.getId(), command.getFirstName(), command.getLastName(), command.getEmail(), command.getSubscription());
    }

    @PostMapping("/workout/{user_id}")
    public void addWorkout(@PathVariable Long user_id, @RequestBody WorkoutCommand command){
        userService.addWorkout(user_id, command);
    }

    @PostMapping("/cancel")
    public User cancelSession(@RequestBody IdCommand command){
        return userService.cancelSession(command.getUserId(), command.getSessionId());
    }

    @PostMapping("/change")
    public void changePassword(@RequestBody ChangePasswordCommand command){
        userService.changePassword(command.getPassword(),command.getId());
    }

    @PostMapping("/image/{id}")
    public void uploadImage(@PathVariable Long id, @RequestParam("image") MultipartFile file){
        userService.saveImageFile(id,file);
    }

    @PostMapping("/diagnostics/{id}")
    public void updateDiagnostics(@PathVariable Long id, @RequestBody DiagnosisCommand command){
        userService.updateDiagnostics(id,command.getDiag());
    }

    @DeleteMapping("/delete/{id}")
    public void deleteUser(@PathVariable Long id){
        userService.deleteUser(id);
    }
}
