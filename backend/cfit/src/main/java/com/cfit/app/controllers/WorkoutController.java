package com.cfit.app.controllers;

import com.cfit.app.commands.IdCommand;
import com.cfit.app.models.Workout;
import com.cfit.app.services.WorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/workout")
public class WorkoutController {

    private WorkoutService workoutService;

    @Autowired
    public WorkoutController(WorkoutService workoutService) {
        this.workoutService = workoutService;
    }

    @GetMapping("/{id}")
    public List<Workout> getWorkouts(@PathVariable Long id){
        return workoutService.findWorkoutsByUserId(id);
    }

    @PostMapping
    public void deleteWorkout(@RequestBody IdCommand command){
        workoutService.deleteWorkout(command.getUserId());
    }
}
