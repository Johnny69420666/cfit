package com.cfit.app.services.Impl;

import com.cfit.app.models.Workout;
import com.cfit.app.repositories.UserRepository;
import com.cfit.app.repositories.WorkoutRepository;
import com.cfit.app.services.WorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class WorkoutServiceImpl implements WorkoutService {

    private final WorkoutRepository workoutRepository;
    private final UserRepository userRepository;

    @Autowired
    public WorkoutServiceImpl(WorkoutRepository workoutRepository, UserRepository userRepository) {
        this.workoutRepository = workoutRepository;
        this.userRepository = userRepository;
    }


    @Override
    public Workout findById(Long l){
        Optional<Workout> workoutOptional = workoutRepository.findById(l);
        if (!workoutOptional.isPresent()){
            throw new RuntimeException("Workout with id " + l + " not found");
        }
        return workoutOptional.get();
    }

    @Override
    public List<Workout> findWorkoutsByUserId(Long l){
        List<Workout> workouts = userRepository.findById(l).get().getWorkouts();
        Comparator<Workout> comparator = Comparator.comparing(Workout::getDate);

        Collections.sort(workouts, comparator.reversed());

        return workouts;
    }

    @Override
    public void deleteWorkout(Long l){
        workoutRepository.delete(findById(l));
    }

}
